package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	MetricsHeaderField  = "bridgedb-metrics-end"
	MetricsVersionField = "bridgedb-metrics-version"
	MetricCountField    = "bridgedb-metric-count"
	MetricsTimeLayout   = "2006-01-02 15:04:05"
	MetricsBinSize      = 10 
	//MetricsBinSize is 10 because we round up the 
    //requests count to the next multiple of 10 to preserve some user privacy.
)

var SupportedVersions = map[string]bool {
    "1": true,
    "2": true,
}

type MetricCount struct {
	Distributor string
	BridgeType  string
	// The request's country code (e.g., "nl"), or email domain (e.g., "gmail").
	RequestOrigin string
	Success       string
	Count         int
}

type MetricsFile struct {
	MetricsWindow time.Duration
	MetricsEnd    time.Time
	Version       int
	MetricCounts  []MetricCount
}

type Filter struct {
	Distributor   *regexp.Regexp
	BridgeType    *regexp.Regexp
	RequestOrigin *regexp.Regexp
	Success       *regexp.Regexp
}

func CreateFilter(distributor, bridgeType, requestOrigin, success string) (*Filter, error) {

	var r *regexp.Regexp
	var err error
	var f = new(Filter)

	if r, err = regexp.Compile(distributor); err != nil {
		return nil, err
	}
	f.Distributor = r

	if r, err = regexp.Compile(bridgeType); err != nil {
		return nil, err
	}
	f.BridgeType = r

	if r, err = regexp.Compile(requestOrigin); err != nil {
		return nil, err
	}
	f.RequestOrigin = r

	if r, err = regexp.Compile(success); err != nil {
		return nil, err
	}
	f.Success = r

	return f, nil
}

func (f *Filter) MatchesAll(mc *MetricCount) bool {

	if !f.Distributor.MatchString(mc.Distributor) {
		return false
	}
	if !f.BridgeType.MatchString(mc.BridgeType) {
		return false
	}
	if !f.RequestOrigin.MatchString(mc.RequestOrigin) {
		return false
	}
	if !f.Success.MatchString(mc.Success) {
		return false
	}
	return true
}

func (mf *MetricsFile) Filter(filter *Filter) *MetricsFile {

	newFile := &MetricsFile{}
	newFile.MetricsWindow = mf.MetricsWindow
	newFile.MetricsEnd = mf.MetricsEnd
	newFile.Version = mf.Version

	for _, mc := range mf.MetricCounts {
		if filter.MatchesAll(&mc) {
			newFile.MetricCounts = append(newFile.MetricCounts, mc)
		}
	}
	return newFile
}

// Sum returns the lower and upper bound of this (filtered) metrics file.
func (mf *MetricsFile) Sum() (int, int) {

	var lower, upper int
	for _, mc := range mf.MetricCounts {
		upper += mc.Count
		lower += mc.Count - MetricsBinSize + 1
	}
	return lower, upper
}

func createMetricsCount(metricCount string) (*MetricCount, error) {

	sstrings := strings.Split(metricCount, " ")
	if sstrings[0] != MetricCountField {
		return nil, fmt.Errorf("expected '%s' but got '%s'", MetricCountField, metricCount)
	}
	r := &MetricCount{}
    
	fields := strings.Split(sstrings[1], ".")
    
    var err error
 
    if strings.HasPrefix(fields[0], "internal") {
        return nil, fmt.Errorf(
            "internal metrics will not be included in plots:'%s'", metricCount)
    }
    
	r.Distributor = fields[0]
	r.BridgeType = fields[1]
	r.RequestOrigin = fields[2]
	r.Success = fields[3]

	r.Count, err = strconv.Atoi(sstrings[2])
	if err != nil {
		return nil, fmt.Errorf("couldn't convert string '%s' to number", sstrings[2])
	}

	return r, nil
}

func extractHeader(scanner *bufio.Scanner, mf *MetricsFile) error {

	// A metrics file must start with the following line:
	// "bridgedb-metrics-end" YYYY-MM-DD HH:MM:SS (NSEC s) NL
	scanner.Scan()
	line := scanner.Text()
	fields := strings.Split(line, " ")
	if fields[0] != MetricsHeaderField {
		return fmt.Errorf("expected file to start with '%s' but got '%s'", MetricsHeaderField, fields[0])
	}

	var b strings.Builder
	fmt.Fprintf(&b, "%s %s", fields[1], fields[2])
	t, err := time.Parse(MetricsTimeLayout, b.String())
	if err != nil {
		return fmt.Errorf("failed to parse time string '%s'", b.String())
	}
	mf.MetricsEnd = t

	// The second line must look as follows:
	// "bridgedb-metrics-version" VERSION NL
	scanner.Scan()
	line = scanner.Text()
	fields = strings.Split(line, " ")
	if fields[0] != MetricsVersionField {
		return fmt.Errorf("expected line to start with '%s' but got '%s'", MetricsVersionField, fields[0])
	}
	if !SupportedVersions[fields[1]] {
		return fmt.Errorf(
            "unsupported metrics version (expected '%v' but got '%s'", 
                SupportedVersions, fields[1])
	}
	mf.Version = 1

	return nil
}

func processMetricsFile(filename string) (*MetricsFile, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	return processMetricsStream(file)
}

func processMetricsStream(fd io.Reader) (*MetricsFile, error) {

	mf := &MetricsFile{}
	scanner := bufio.NewScanner(fd)
	if err := extractHeader(scanner, mf); err != nil {
		return nil, err
	}

	mcs := []MetricCount{}
	for scanner.Scan() {
		mc, err := createMetricsCount(scanner.Text())
		if err != nil {
			log.Printf("Skipping line: %s", err)
			continue
		}
		mcs = append(mcs, *mc)
	}
	mf.MetricCounts = mcs

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return mf, nil
}

func main() {

	// Command line flags.
	var filename string
	var distributor string
	var bridgeType string
	var requestOrigin string
	var success string
	var stdin bool
	var verbose bool

	flag.StringVar(&filename, "f", "", "File to process")
	flag.StringVar(&distributor, "d", "", "Distribution mechanism (i.e., 'https', 'email', or 'moat')")
	flag.StringVar(&bridgeType, "b", "", "Bridge type (i.e., 'obfs2', 'obfs3', 'obfs4', ...)")
	flag.StringVar(&requestOrigin, "o", "", "Request origin as country code or email provider (i.e., 'ca', 'it', ..., 'gmail', 'riseup')")
	flag.StringVar(&success, "s", "", "Success (i.e., 'success' or 'fail')")
	flag.BoolVar(&verbose, "v", false, "Show matching metric-count lines")
	flag.BoolVar(&stdin, "i", false, "Process data from stdin instead of from a file.")
	flag.Parse()

	if filename == "" && stdin == false {
		log.Fatal("Use the '-f' flag and point me to a BridgeDB metrics file or '-i' and I'll read from stdin.")
	}

	if filename != "" && stdin == true {
		log.Fatal("Use either '-f' or '-i' but not both.")
	}

	var mf *MetricsFile
	var err error
	if filename != "" {
		// Parse the given metrics file.
		mf, err = processMetricsFile(filename)
		if err != nil {
			log.Fatal(err)
		}
	} else if stdin != false {
		// Parse stdin.
		mf, err = processMetricsStream(os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	}

	// Create filter expressions from the given command line arguments.
	filter, err := CreateFilter(distributor, bridgeType, requestOrigin, success)
	if err != nil {
		log.Fatal(err)
	}

	// Filter the metrics file according to the given filter expressions.
	mf = mf.Filter(filter)
	if verbose {
		for _, mc := range mf.MetricCounts {
			log.Println(mc)
		}
	}
	lower, upper := mf.Sum()
    fmt.Printf("%s,%d,%d\n", mf.MetricsEnd, lower, upper)
}
