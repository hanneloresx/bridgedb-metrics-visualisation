# BridgeDB metrics parser

## Requirements

* R plus the modules ggplot2 and optparse.
* Golang.

## How to create diagrams

1. Fetch BridgeDB metrics files from
   [CollecTor](https://collector.torproject.org/recent/bridgedb-metrics/):

        make data

2. Compile the binary `bridgedb-metrics-parser.go`:

        make build

2. Create plots:

        make plots

## How to add new diagrams

1. Edit the file `creats-plots.sh` and use one of the existing analysis blocks
   as template.

2. Re-create the plots:

        make plots
